Voici la liste des travaux en cours par vthierry et cie sur le portail : https://mooc-nsi-snt.gitlab.io/portail/index.html

# Travail vthierry en cours
- revoir les pulldown dans les FAQs.

# Points à voir avec Marie-Hélène et Isabelle
- Valider les contenus actuels !
- Ajouter des illustrations de décoration ?

# Points à voir avec Benoit
- 

Ref:
- https://gitlab.com/mooc-nsi-snt/portail/-/blob/master/makefile.md ce "pad" todoliste
- https://gitlab.com/mooc-nsi-snt/portail/-/blob/master/Aide/Exemples.md pour avoir des exemples de .md
