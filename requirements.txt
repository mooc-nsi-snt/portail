mkdocs
mkdocs-material
mkdocs-jupyter
mkdocs-macros-plugin
mkdocs-exclude
mkdocs-awesome-pages-plugin
mkdocs-redirects
mkdocs-page-pdf==0.0.6