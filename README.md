# Portail Moocs NSI

Ce dépôt contient la configuration et les templates du portail des moocs NSI.

## Organisation des fichiers

Les fichiers markdown sont compilés depuis le dossier `/docs`. 
Les moocs 1 et 2 sont inclus via des sous-modules git.

```bash
📦portail # --------------------------- Root directory
 ┣ 📂docs # --------------------------- Website sources
 ┃ ┣ 📂3_Les_Fondamentaux # ----------- Folder Mooc "Les Fondamentaux" (sous-modules initialisé dans Le_Mooc/)
 ┃ ┣ 📂4_Apprendre_A_Enseigner # ------ Folder Mooc "Apprendre à enseigner" (sous-modules initialisé dans Le_Mooc/)
 ┃ ┣ 📂assets # ----------------------- General assets (logo, favicon, etc)
 ┃ ┣ 📜.pages # ----------------------- Folder config file (plugin mkdocs awesome pages)
 ┃ ┣ 📜*.md # ------------------------- Pages content file
 ┃ ┗ 📜index.md # --------------------- Homepage content file (overriden by home.html)
 ┣ 📂overrides # ---------------------- Custom pages overrides
 ┃ ┗ 📜home.html # -------------------- Homepage override
 ┣ 📜.gitignore # --------------------- Git ignored files
 ┣ 📜README.md # ---------------------- Main readme file (This file you're reading)
 ┣ 📜.git* # -------------------------- Git and gitlab configuration files
 ┗ 📜mkdocs.yml # --------------------- Website main config file
```

## Déploiement local

Afin de contribuer et visualiser vos modifications, vous pouvez déployer le site localement 
sur votre ordinateur.

### Installation

**Prérequis :**
- Installer Miniconda : https://docs.conda.io/en/latest/miniconda.html
- Installer les dépendances :
  ```shell
  pip install mkdocs-material mkdocs-jupyter mkdocs-macros-plugin mkdocs-awesome-pages-plugin mkdocs-redirects mkdocs-page-pdf
  ```

### Visualisation en local

1. Cloner le dépôt avec les sous-modules (moocs 1 & 2)
  ```shell
  git clone --recursive git@gitlab.com:mooc-nsi-snt/portail.git # ssh
  # ou
  git clone --recursive https://gitlab.com/mooc-nsi-snt/portail.git # https si aucune clé ssh configurée
  ```
2. Mettre à jour les sous-modules (moocs 1 & 2, à faire à chaque fois que les sous-modules ont été modifié)
  ```shell
  git submodule update --remote --merge
  ```
3. Se placer dans le projet à la racine et lancer le serveur mkdocs
  ```shell
  mkdocs serve
  ```

Après quelques secondes, le site devrait être visible ici : http://localhost:8000
