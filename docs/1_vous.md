---
title: Pour qui et pour quoi ?
description: Présentation de l'offre et du public
hide:
- navigation
---

L'introduction de l'enseignement de l'informatique au lycée va permettre aux prochaines générations de maîtriser et participer au développement du numérique, et non pas uniquement de le consommer. Le principal enjeu est alors la formation des enseignantes et des enseignants : cet espace y contribue.

## Pour qui ?

 Pour vous :)

### À qui s'adressent ces ressources ?
 
À toutes les professeures et tous les professeurs d’informatique francophones qui veulent ou doivent se former et préparer, puis évaluer leurs enseignements.

Plus précisément au niveau "secondaire" francophones (voir [ci-dessous](https://mooc-nsi-snt.gitlab.io/portail/1_vous.html#quelles-sont-les-terminologies-des-classes-du-secondaire-au-sein-du-monde-francophone), pour la terminologie).

En France, cela correspond aux collègues des lycées généraux et techniques enseignant
- les _sciences numériques et technologie_ ([SNT](https://fr.wikipedia.org/wiki/Sciences_num%C3%A9riques_et_technologie), en [seconde](https://www.education.gouv.fr/bo/19/Special1/MENE1901641A.htm)), qui permettent à tou·te·s de s'initier aux fondements du numérique et
- l'enseignement de spécialité _numérique et sciences informatiques_ (NSI) en [1ère](https://www.education.gouv.fr/bo/19/Special1/MENE1901633A.htm) et [Terminale](https://www.education.gouv.fr/bo/19/Special8/MENE1921247A.htm).

#### et aussi …

Au delà des enseignantes et enseignantes en poste ou en devenir, ces ressources s'adressent :

- aux _parents_ et tout acteur ou actrice de l'éducation désirant suivre et accompagner cette partie inédite des études que suivent les élèves désormais,
- aux _citoyennes et citoyens_ qui n'ont pas eu la chance de profiter de ces enseignements et en ont besoin au quotidien (par exemple parce que participant à un projet informatique),
- aux _cadres_ de l'éducation qui ont besoin d'avoir une vision précise de ces enseignements.

### Qui a vocation à enseigner l'informatique ?

On identifie trois grandes catégories de collègues ou collègues en devenir:

- Les _étudiantes et étudiants_ en informatique (universitaires ou en école d'ingénérie), qui feront le choix de devenir enseignants : elles et ils ont un bon niveau informatique et ont surtout besoin d'acquérir des compétences en [pédagogie](https://fr.wikipedia.org/wiki/P%C3%A9dagogie) et en [didactique de l'informatique](https://fr.wikipedia.org/wiki/Didactique).
- Les _collègues_ enseignant d'autres disciplines, souvent scientifiques (mais pas exclusivement), qui veulent varier de discipline : illes sont au fait de la pédagogie, doivent adapter la didactique de leur discipline à celle de l'informatique et doivent acquérir les bases des sciences et techniques informatiques, puis se former à cette discipline.
- Les _professionnels et professionnelles_ de l’informatique, souvent très pointues dans un sous-domaine de l'informatique, doivent souvent élargir leurs compétences à d'autres sous-domaine, et ont besoin d'acquérir des compétences en [pédagogie](https://fr.wikipedia.org/wiki/P%C3%A9dagogie) et en [didactique de l'informatique](https://fr.wikipedia.org/wiki/Didactique).

Bien entendu, celà n'est pas exclusif.

## Pour quoi ?

Pour obtenir deux classes de compétences : 

- (i) un _niveau en informatique théorique et pratique_ suffisant pour enseigner et prendre de la hauteur par rapport à cet enseignement
- (ii) une _capacité à enseigner ces savoirs, savoir-faire et savoir-être_ par rapport à l'informatique et au numérique.

### Que nous offre ces ressources ?

1. Une [formation disciplinaire](https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux.html) d'initiation, de formation à la programmation, et de formation aux fondamentaux de l'informatique.
2. Une [formation didactique](https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner.html) pour apprendre, ensemble à enseigner, 
    - qui inclut aussi des ressources pour la [préparation au CAPES](https://www.societe-informatique-de-france.fr/capes-informatique).
3. Un [espace de partage et d'entraide](https://mooc-nsi-snt.gitlab.io/portail/5_Forum.html) pour être accompagner pendant la formation, la préparation et l'évaluation des enseignements

avec une invitation à [faire communauté](https://mooc-nsi-snt.gitlab.io/portail/6_Communaute.html) et bien entendu la [possibilité de contribuer](https://mooc-nsi-snt.gitlab.io/portail/7_Contribuer.html) à ces ressources partagées.

### Comment s'utilisent ces ressources ?

- En _auto-formation_ vous pouvez suivre les deux MOOCs qui seront disponibles à la rentrée 2021 et vous former individuellement ou en petite équipe d'éntraide, puis aller sur les forums et participer aux rencontres en ligne pour consolider et valider vos acquis.

- De _manière hybride_ ces ressources sont utilisées au sein de formation continue ou initiales, sous forme de travail personnel en ligne préalable, suivi de "classes" présentielles. Ce sera par exemple le cas du Master MEEF-Info de l'[INSPÉ de l'académie de Nice](https://www.reseau-inspe.fr/annuaires/inspe-france/inspe-academie-nice).

## Vos principales questions !

### Ces formations sont elles payantes, chronophages, réutilisables ? 

Coûteuses en argent non, en temps oui. 

- Nous construisons ici des biens communs, librement utilisables et réutilisables 
   - Ils sont sous licence [CC-BY](https://creativecommons.org/licenses/by/4.0/deed.fr) ou [CC-BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr) selon le choix des autrices ou auteurs, et [CeCiLL-C](https://cecill.info/licences/Licence_CeCILL-C_V1-fr.html) pour les composants logiciels.



Rien n'est gratuit, mais ces ressources ont été financées par notre argent public, et aussi beaucoup de temps et d’énergie de collègues enseignantes et enseignants du secondaire et du supérieur, au delà de leur missions et temps de travail, pour en faire un bien commun non-commercial, de façon à être accessible à toutes et tous, au delà des frontières aussi.

Mais _oui_ c'est un véritable investissement en temps, qui correspond à une formation universitaire de _deux ans à temps plein_ (on parle de 400 heures de travail cumulé, pour une personne qui débuterait sur tout les aspects). En pratique il faut étaler le suivi de ces formations sur une année voir deux ans, et prévoir quelques heures par mois pour avancer.

Ces ressources sont donc pleinement  réutilisables en totalité ou en partie, avec seule obligation de les citer, et une forte incitation à nous informer de leur réutilisation: cela permettra de vous aider et cela nous permettra de montrer leur usage.

#### Pour nous citer :   

<img alt="Licence Creative Commons" style="border-width: 0px;" data-mce-src="https://i.creativecommons.org/l/by/4.0/88x31.png" dfsrc="https://i.creativecommons.org/l/by/4.0/88x31.png" src="https://i.creativecommons.org/l/by/4.0/88x31.png" saveddisplaymode=""></a></span><br><b>Numérique et Sciences Informatiques : les fondamentaux de </b><span class="Object" role="link" id="OBJ_PREFIX_DWT197_com_zimbra_url"><a href="https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/" rel="cc:attributionURL nofollow noopener noreferrer" target="_blank">https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/</a></span> est mis à disposition selon les termes de la <span class="Object" role="link" id="OBJ_PREFIX_DWT198_com_zimbra_url"><a rel="license nofollow noopener noreferrer" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">licence Creative Commons Attribution 4.0 International</a></span>.<br>Les autorisations au-delà du champ de cette licence peuvent être obtenues à <span class="Object" role="link" id="OBJ_PREFIX_DWT199_com_zimbra_url"><a href="https://mooc-nsi-snt.gitlab.io/portail/0_Accueil.html" rel="cc:morePermissions nofollow noopener noreferrer" target="_blank">https://mooc-nsi-snt.gitlab.io/portail/0_Accueil.html</a></span>.


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><b>Apprendre à enseigner le Numérique et les Sciences Informatiques</b></span> de <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.fun-mooc.fr/fr/cours/apprendre-a-enseigner-le-numerique-et-les-sciences-informatiques/" property="cc:attributionName" rel="cc:attributionURL">https://www.fun-mooc.fr/fr/cours/apprendre-a-enseigner-le-numerique-et-les-sciences-informatiques/?edit</a> est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">licence Creative Commons Attribution 4.0 International</a>.<br />Fondé(e) sur une œuvre à <a xmlns:dct="http://purl.org/dc/terms/" href="https://mooc-nsi-snt.gitlab.io/portail/0_Accueil.html" rel="dct:source">https://mooc-nsi-snt.gitlab.io/portail/0_Accueil.html</a>.


### Quelles sont les terminologies des classes du secondaire au sein du monde francophone ?

Nous ciblons les cours d’informatique du niveau "secondaire" francophones au sens suivant:

| France  | Belgique  | Suisse | Québec | Allemagne| USA  |
| ---    |  ---     | ---    | ---    | ---       | --- |
| _(Lycée)_ | _(Secondaire)_ | _(Secondaire II)_ | _(Secondaire)_ | _(Oberstufe)_| _(High School)_ |
| 2nd    | 4ème     |  12ème  | IV     | 10ème     | K10 |
| 1ère   | 5ème Poésie |  13ème   | V DES    | 11ème     | K11 |
| Terminale | 6ème Réthorique |  14ème   | Préuniversitaire     | 12ème     | K12 |

selon les correspondances que l'on le lit [ici](https://fr.wikipedia.org/wiki/Comparaison_entre_le_syst%C3%A8me_d%27%C3%A9ducation_belge_et_d%27autres_syst%C3%A8mes_%C3%A9ducatifs) ou [là](https://sissevres.org/fr/content/tableau-d%C3%A9quivalence-de-niveau). La "2nd" correspond à la 10ème année d'étude depuis l'apprentissage de la lecture.
      
<script src="./assets/make_foldable.js"/>

