---
title: Contact
description: Contactez-nous
hide:
- navigation
---

Rechercher une ressource ? Proposer une ressource ? Demander de l'aide sur une ressource ? Solliciter une aide en direct ? Poser une question ? Être mis·e en relation avec un·e collègue ? Contribuer au projet ?

Ce “bureau d’accueil” est ouvert à toutes et tous : enseignant·e·s et autres professionnel·le·s de l’éducation, curieux de science, et citoyen·ne·s.

Nous avons aussi un “salon en ligne” si vous avez besoin (un système de visio-réunion); pour l’utiliser, contactez-nous ici.

<center>
<table>
<tr style="background-color:lightgray">
<td align="center"><b>Mail</b></td>
<td align="center"><b>Twitter</b></td>
<td align="center"><b>Facebook</b></td>
<td align="center"><b>Tel</b></td>
</tr>
<tr>
<td><a href="mailto:cai-contact@inria.fr?subject=Ma%20question%20à%20propos%20de%20la%20Communauté%20d'Apprentissage%20de%20l'Informatique&amp;body=%20Ma%20question%20:%20%0A%0A%20Qui%20je%20suis%20%20:" title="cai-contact@inria.fr"><img alt="contact-mail" src="https://mooc-nsi-snt.gitlab.io/portail/./assets/contact-mail.png" /></a></td>
<td><a href="https://twitter.com/intent/tweet?source=webclient&amp;text=À+propos+de+https://www.cai.community+@CaiCommunity" title=""><img alt="contact-twitter" src="https://mooc-nsi-snt.gitlab.io/portail/./assets/contact-twitter.png" /></a></td>
<td><a href="https://www.facebook.com/Cai-community-111060377299220" title=""><img alt="contact-facebook" src="https://mooc-nsi-snt.gitlab.io/portail/./assets/contact-facebook.png" /></a></td>
<td><a href="#" title="Au téléphone aussi ;) +33 4 92 38 76 88"><img alt="contact-tel" src="https://mooc-nsi-snt.gitlab.io/portail/./assets/contact-tel.png" /></a></td>
</tr>
</table></center>

Vous pouvez aussi vous exprimer plus précisément [via un formulaire](https://cai.community/contact/#frm_form_2_container).
