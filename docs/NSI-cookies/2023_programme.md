# NSI-cookies : l'offre 2023

<table>
<tbody>
<tr>
<td width="20%"><a href="http://terra-numerica.org/"><img class="aligncenter" src="https://terra-numerica.org/files/2020/10/logo-TN-transp.png" alt="Terra Numerica"  /></a></td>
<td width="20%"><a href="https://mooc-nsi-snt.gitlab.io/portail/6_Communaute.html"><img class="aligncenter" src="https://gitlab.com/mooc-nsi-snt/portail/-/raw/master/assets/logo-nsi-cookies.png" alt="funny cooky'faces"  /></a></td>
<td width="20%"><a href="https://learninglab.inria.fr/"><img class="aligncenter" src="https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2022/11/logo-learninglab-inria.png" alt="Learning Lab Inria" /></a></td>
<td width="20%"><a href="https://aeif.fr/"><img class="aligncenter" src="https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2022/11/logo-aeif.png" alt="Learning Lab Inria" /></a></td>
<td width="20%">
Voici le programme 2023 de notre initiative <b><a href="https://mooc-nsi-snt.gitlab.io/portail/6_Communaute.html">NSI cookies</a></b>, attention ne le construisons au fil du temps en retour à l'expression de vos besoins :) </td></tr>
</tbody></table>

## Nos archives

- Mercredi 28 juin, 19h-20h Webinaire : Découvrir la forge https://gitlab.com/mooc-nsi-snt, comme outil de construction de ressources pédagogiques sous forme de commun numérique (textes, sites web, logiciels, …), présentation de ressources existantes en SNT/NSI et partage d'inventaires de ressources, lien avec le logiciel libre en éducation,
     - avec la participation de [Vincent-Xavier Jumel](https://www.linkedin.com/in/vincent-xavier-jumel) et [Charles Poulmaire](https://www.linkedin.com/in/charles-poulmaire-7957b482/)
      - La séance aura lieu en ligne  [sur ce lien](https://visio-agents.education.fr/meeting/signin/159585/creator/800/hash/f1bfa1fe0c9724a35ba2cf9ba4f742ab6232db7f) et sera enregistrée et partagée [comme les autres webinaires](https://www.youtube.com/playlist?list=PLKGPGznq6a-Veb8AAaief6sm2qUGM12GE).

- Mercredi 14 juin, 19h-20h Webinaire : présentation, fonctionnement et utilisation de [ChatGPT](https://fr.wikipedia.org/wiki/ChatGPT)  … comprendre par la [pratique](https://chat.openai.com/chat), comment trouver des modalités d'évaluation pour concilier utilisation et travail perso, que faut-il [enseigner dans un monde où Chat GPT est dispobible](https://www.societe-informatique-de-france.fr/2023/03/chatgtp-et-les-modeles-generatifs) ?
    - Une présentation scientifique (super pédagogique pour les enseignant·e·s) de [ChatGPT est proposée sur ce lien](https://www.lemonde.fr/blog/binaire/?p=14643).
   - Voici la [rediffusion](https://youtu.be/MMI7Ft896zc)
 
- Mercredi 31 mai, 19h-20h Webinaire : utilisation du [ESP32](https://fr.wikipedia.org/wiki/ESP32) pour aborder la notion d'objet connecté et d'informatique enfouie et l'utilisation d'architectures matérielles en absence de systèmes d'exploitation.
   - Voici la [rediffusion](https://youtu.be/Oq0hnRreQKY)

- Mercredi 3 mai, 19h-20h Webinaire : utilisation du [Raspberry Pi](https://fr.wikipedia.org/wiki/Raspberry_Pi) avec la carte [SenseHat](https://www.kubii.fr/modules-capteurs/1081-raspberry-pi-sense-hat-kubii-640522710799.html). Ce qui est intéressant c'est que l'on peut faire travailler les élèves sur des simulateurs puis ensuite tester leurs prgrammes, idées et/ou projet sur le matériel. 
     - Voici la [rediffusion](https://youtu.be/tuRJatjPBp8).
     - _Nous pouvons en discuter sur [cette catégorie](https://mooc-forums.inria.fr/moocnsi/t/utilisation-du-raspberry-pi-avec-la-carte-sensehat-en-snt-et-nsi-un-webinaire-de-formation-et-de-partage-de-pratiques/9009) du forum._

- Mercredi 17 mai : **séance annulée et reportée au 21 juin**.

- Mercredi 8 mars, 14h-17h séance présentielle sur le secteur de Toulon et Sophia-Antipolis
    - 1/ _Comment enseigner la géolocalisation en SNT ?_ rappel du thème + discussions sur les points de contenu ou de didactique à lever + propositions d'activités et expérimentation
         - Voici la [rediffusion](https://youtu.be/28Kh86-L1p0).
         - Voici les [slides de la présentation](https://view.genial.ly/6405f5184ea626001a1fe211/interactive-content-nsicookies-cartographie)avec des liens vers des ressources et éléments de référence.
         - Pour en savoir plus https://classcode.fr/snt chapitre géolocalisation
         - Voir aussi des ressources des enseignants comme le travail de [Stephan Vanzuijl](https://start.me/p/jj0z46/localisation-cartographie) ou de [Julien Launay](http://icnisnlycee.free.fr/index.php/43-snt/localisation-cartographie-et-mobilite) ou [David Roche](https://dav74.github.io/site_snt/a22/) etc...
    - 2/ _Comment améliorer l'égalité des genres en informatique ?_ : rappel de quelques données (statistiques de la [DEPP](https://archives-statistiques-depp.education.gouv.fr/), état des lieux de la [SIF](https://www.societe-informatique-de-france.fr/)) + présentation rapide de travaux de [Marie Duru-Bellat](https://fr.wikipedia.org/wiki/Marie_Duru-Bellat) après [Pierre Bourdieu](https://fr.wikipedia.org/wiki/Pierre_Bourdieu) et de [Isabelle Collet](https://fr.wikipedia.org/wiki/Isabelle_Collet) sur la [pédagogie de l'égalité](https://interstices.info/appliquer-une-pedagogie-de-legalite-dans-les-enseignements-dinformatique/) ; tour de table et partage de ressources pratiques concrètes.
         - Voic la [rediffusion](https://youtu.be/r3eiMU5cyuQ).
         - Voici les [slides de la présentation](hhttps://view.genial.ly/640612954ea626001a2038ad/interactive-content-nsicookies-femmes-and-informatiques) avec des liens vers des ressources et éléments de référence.
         - Pour en savoir plus le chapitre du [MOOC NSI](https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner/Le_Mooc/3_Prendre-du-recul-au-niveau-didactique/3.3_Pedagogie-de-l-egalite/0_Introduction.html) sur la pédagogie de l'égalité.

- Jeudi 16 février , 19h-20h Webinaire : utiliser [micro:bit](https://makecode.microbit.org/) et faire une [IHM](https://fr.wikipedia.org/wiki/Interactions_homme-machine) utilisation en SNT, mais aussi en NSI avec les projets
    - Le Webinaire est [disponible en rediffusion](https://youtu.be/28Kh86-L1p0) dans le MOOC NSI

- Jeudi 2 et vendredi 3 février : présentation de l'initiative [NSI-Cookies](https://mooc-nsi-snt.gitlab.io/portail/6_Communaute.html) lors des journées NSI de l'Académie de Nice.

- Mardi 24 janvier , 19h-20h Webinaire_ : à la découverte de [Capytale](https://ent2d.ac-bordeaux.fr/disciplines/nsi/2022/06/28/capytale/) qui est un environnement de programmation Python entièrement en ligne (sans aucune installation, utilisant [Basthon](https://basthon.fr/)) qui permet de travailler avec les élèves.
    - Le Webinaire est [disponible en rediffusion](https://youtu.be/9SZHRdKrgmI)  et dans le MOOC NSI

<hr/>

- Autres idées pour une future action:
     - sur les bases de données, on pourra suivre le module 1.4 du [MOOC NSI sur les fondamentaux](https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/index.html) accès direct [ici](https://lms.fun-mooc.fr/courses/course-v1:inria+41028+session01/courseware/d21b1c9614df48569d7073009beea699/be434df1f8be484785b3161c1752018f/) pour les personnes inscrites,
    - sur un thème plus sociétal comme les réseaux sociaux ou les fake news en lien avec les thèmes SNT sur les réseaux sociaux et le web,
    - le travail en projet (organisation, évaluation), dans le cadre de l'enseignement de l'informatique, en lien avec le MOOC sur [Apprendre à enseigner](https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner), on verra les enjeux, les limites et les leviers pour réussir à faire travailler en projet, à partir d'exemples.
     - Exemples de projets en [première NSI](https://dav74.github.io/site_nsi_prem/) et [terminale NSI](https://dav74.github.io/site_nsi_term/)
   - La séance aura lieu simultanément à
         -  l'[INSPÉ de la Seyne sur Mer]( https://goo.gl/maps/1TdG6igXNwUa2JbN9), 59 All. Émile Pratali, 83500 La Seyne-sur-Mer
         - [TerraNumerica@Sophia](https://terra-numerica.org/terra-numerica-sophia/) situé [18 Rue Frédéric Mistral, 06560 Valbonne](https://goo.gl/maps/88z3ws6XUUcEfG2CA).
         - En ligne  [sur ce lien](https://visio-agents.education.fr/meeting/signin/159585/creator/800/hash/f1bfa1fe0c9724a35ba2cf9ba4f742ab6232db7f), la partie visio sera enregistrée comme un webinaire.

