---
title: Équipe et partenaires
description: 
hide:
- navigation
---

## Qui sommes nous ?

Nous sommes une [équipe](https://tinyl.io/41Ii) de professeures et professeurs d'informatique, d'enseignantes et enseignants chercheurs dans ce domaine, d'ingénieures et ingénieurs pédagogiques et développement. Nous nous rassemblons pour identifier les besoins, rassembler ou créer les ressources qui vont pouvoir aider les collègues et futurs collègues.

Nous travaillons en mode projet et co-animons forum et communauté. Vous allez nous croiser au fil des discussions et des ressources.

## Les acteurs

Le projet est porté et opéré par le [Learning Lab Inria](https://learninglab.inria.fr) avec la [Communauté d'Apprentissage de l'Informatique](https://cai.community) en appui :

<table>
<tr>
<td style="width:50%;margin:0 auto;vertical-align:middle"><img alt="Learning Lab Inria" src="https://learninglab.gitlabpages.inria.fr/serious-game/smartphone/img/LogoILL.png" title="https://learninglab.inria.fr"width="300"/></td>
<td style="width:50%;margin:0 auto;vertical-align:middle"><img alt="Communauté d'Apprentissage de l'Informatique" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxwEJTccfjgIPZ_p75k2FgXj24JV_R2TYbwQ&s" title="https://cai.community"/></td>
</tr>
</table>

en partenariat avec des collègues des établissements d'enseignement supérieur et de recherche ou associatif suivants :

<table>
<tr>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://ens-paris-saclay.fr"><img src="https://www.sorbonne.fr/wp-content/uploads/ENSPS_UPSAY_logo_couleur_2-690x158.png" alt="ENS Paris Saclay" title="ENS Paris Saclay"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="http://www.ulb.ac.be"><img src="https://www.ictjob.be/wp-content/uploads/2021/04/LOGO.jpg" alt="Université Libre de Bruxelles" title="Université Libre de Bruxelles"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.univ-reunion.fr"><img src="https://upload.wikimedia.org/wikipedia/fr/a/ad/Logo_Université_de_La_Réunion.jpg" alt="Université de la Réunion" title="Université de la Réunion"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://univ-cotedazur.fr"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Logo_université_côte_azur.png/1200px-Logo_université_côte_azur.png" alt="Université Côte d'Azur" title="Université Côte d'Azur"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="http://aeif.fr"><img src="https://pedagogie.ac-lille.fr/numerique-et-sciences-informatiques/wp-content/uploads/sites/45/2021/10/logo-aeif.jpg" alt="Association des enseignantes et enseignants d'informatique de France" title="Association des enseignantes et enseignants d'informatique de France"></a></td>
</tr>
<tr>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.universite-paris-saclay.fr"><img src="https://www.universite-paris-saclay.fr/sites/default/files/media/2019-12/logo-ups.svg" alt="Université Paris Saclay" title="Université Paris Saclay"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.univ-grenoble-alpes.fr/"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Logo_Université_Grenoble_Alpes_2020.svg/1200px-Logo_Université_Grenoble_Alpes_2020.svg.png" alt="Université Grenoble Alpes" title="Université Grenoble Alpes"></a></td>
<td style="width:20%;margin:0 auto;vertical…auto;vertical-align:middle"><a href="https://www.liglab.fr"><img src="https://www.liglab.fr/sites/default/files/theme/logo-lig_0.svg" alt="Laboratoire d'Informatique de Grenoble" title="Laboratoire d'Informatique de Grenoble"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.umontpellier.fr/"><img src="https://www.udice.org/wp-content/uploads/2024/07/logo_um_2022_rouge_RVB.png" alt="Université de Montpellier" title="Université de Montpellier"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.cyu.fr/"><img src="https://www.sorbonne.fr/wp-content/uploads/CY-Cergy-Paris-Universite_coul.jpg" alt="Université de Cergy Pontoise" title="Université de Cergy Pontoise"></a></td>
</tr>
</table>

sous l'égide de la [Société Informatique de France](https://www.societe-informatique-de-france.fr) avec l'[EPI](https://www.epi.asso.fr/) et de l'association [Class´Code](https://classcode.fr) pour l'ancrage territorial avec pour le projet [NSI-cookies](https://mooc-nsi-snt.gitlab.io/portail/6_Communaute.html):

<table>
<tr>
<td style="width:50%;margin:0 auto;vertical-align:middle"><a href="http://terra-numerica.org/"><img width="300" src="http://terra-numerica.org/files/2020/10/cropped-logo-TN-transp.png" alt="Terra Numerica"></a></td>
</tr>
</table>

## Avec le soutien …

… d'[Inria](https://www.inria.fr) et de la [Direction du Numérique pour l’Éducation](https://www.education.gouv.fr/direction-du-numerique-pour-l-education-dne-9983), l'[Université Ouverte des Humanités](https://uoh.fr/) associée à l'[Université Numérique Ingénierie et Technologie](https://unit.eu) et celui d'[Inria](https://www.inria.fr) :

<table>
<tr>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.societe-informatique-de-france.fr"><img src="https://www.socinfo.fr/uploads/2022/12/logo-sif-2022-vertical-1.svg" alt="Société Informatique de France"></a><br/><br/><br/><a href="https://www.epi.asso.fr/"><img src="https://pbs.twimg.com/profile_images/591174882106462208/GX91gPiT_400x400.jpg" alt="Société Informatique de France"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://classcode.fr"><img src="https://project.inria.fr/classcode/files/2016/07/LOGO-ClassCode-coul.jpg" alt="Class´Code"></a></td>
<td style="width:15%;margin:0 auto;vertical-align:middle"><a href="https://www.education.gouv.fr/direction-du-numerique-pour-l-education-dne-9983"><img src="https://www.education.gouv.fr/sites/default/files/site_logo/2024-12/MENESR.png" alt="Direction du Numérique pour l’Éducation"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://unit.eu/"><img src="https://upload.wikimedia.org/wikipedia/commons/7/75/Logo_UNIT.svg" alt="Université Numérique Ingénierie et Technologie"></a><br/><br/><br/>
<a href="https://uoh.fr/"><img src="https://upload.wikimedia.org/wikipedia/fr/d/dc/Logo_UOH.svg" alt="Université Ouverte des Humanités"></a></td>
<td style="width:20%;margin:0 auto;vertical-align:middle"><a href="https://www.inria.fr"><img src="https://project.inria.fr/populate2025/files/2024/06/RF-Inria_Logo_RVB.png" alt="Inria"></a></td>
</tr>
</table>

## Vos questions !

### Quels sont les documents de pilotage du projet ?

- En toute transparence voici nos documents de travail:
    - Le [document de référence](https://tinyl.io/416C) du projet et une [présentation préliminaire](https://tinyl.io/416B) fin 2020, qui décrit aussi la gouvernance, le budget, etc...
    - Un [inventaire des contenus prévus](https://tinyl.io/416D) et une [structuration provisoire](https://docs.google.com/document/d/1zrt_5cG8SqDLChbuk8RKY7_g19FWP82DsX3UBbuGE_E/edit) qui servent de base au travail en cours.

### Comment participer au projet lui-même ?

-  Bienvenue ! Il suffit de [nous contacter](./7_Contact.html), effectivement notre équipe grandit régulièrement et c'est tant mieux car la tâche est immense.</li>

- Participer officiellement à ce projet est aussi une belle reconnaissance pour ces collègues qui se sont investis à fond depuis des mois pour que cet enseignement réusisse : c'est un levier pour faire connaitre et partager ces contributions.</li>

### Quels sont les éléments de bilan ?

#### Publications de bilan du projet

Aurélie Laggarigue et Sherazade Djeballah,  Octobre 2023, Présentation au CA d’UOH.  [Bilan des formations NSI hybrides et communautaires](https://docs.google.com/document/d/1034cQ0Xjfv0Je__BNvcE86yLOX9fbhj-lyXB8THnlSo/edit?usp=sharing)

Marie-Hélène Comte, Sherazade Djeballah, Maxime Fourny, Sébastien Hoarau, Anthony Juton, et al.. Un espace de formation francophone des enseignants, dédié à l’apprentissage de l’informatique, dans le secondaire.. RR-9514, Inria. 2023, pp.15. ⟨[hal-04140155](https://inria.hal.science/hal-04140155v1)⟩

  - Publié aussi dans  Adjectif : analyses et recherches sur les TICE, 2023, pp.1-5. [⟨hal-04350362⟩](https://hal.science/hal-04350362) et à Didapro9 2022, May 2022, Le Mans, France. [⟨hal-03643914v2⟩](https://inria.hal.science/hal-03643914v2)
  - Avec les données ouvertes et document suivantes:
     - [Présentation du projet](https://docs.google.com/document/d/14N-F4wKsf7D8OWsJ98sFseJpQgDwkJQa4cPqaTkdoDs/edit?usp=drive_link)
     - [Bilan-MOOC-NSI-au-1-Janvier-2023](https://drive.google.com/file/d/1QVup0IdARP5cdJMe7ECNFkjSnnI6cqsI/view?usp=drive_link)
     - [Bilan-MOOC-NSI-au-1-Janvier-2024](https://drive.google.com/file/d/1JmiU0FkjArLQ5DQ4fYbx7tCgdFu1Tq85/view?usp=drive_link)
     - [Questionnaire-NSI-Cookies-Janvier-2023](https://drive.google.com/file/d/1gnISZnawmqHqk-pJo4dXQoXenoBwl-g4/view?usp=drive_link) 

__Portail de l’espace de formation, dédié à l'apprentissage de l'informatique (ce portail):__

        [https://mooc-nsi-snt.gitlab.io/portail](https://mooc-nsi-snt.gitlab.io/portail) 

#### Publications large public

- Ce [teaser](https://youtu.be/QTjXRIwF4Vc) présente la formation en 160 secondes.
- Ce [flyer](https://mooc-nsi-snt.gitlab.io/portail/flyer-nsi.pdf) présente la formation en un coup d'oeil.

Charles Poulmaire, Sherazade Djeballah avec Ikram Chraibi-Kaadoud, Thierry Viéville [Tu maîtriseras l’informatique, ma fille, mon fils](https://www.lemonde.fr/blog/binaire/2023/09/29/tu-maitriseras-linformatique-ma-fille-mon-fils). Blog binaire sur LeMonde.fr, Septembre 2023

Aurélie Lagarrigue, Charles Poulmaire, Marie-Hélène Comte, Sabrina Barnabé, Stéphane Renouf, Thierry Viéville, [La communauté des SNT et NSI « cookies »](https://www.lemonde.fr/blog/binaire/2022/12/02/la-communaute-des-snt-et-nsi-cookies), Blog binaire sur LeMonde.fr, Décembre 2022

Jean-Marc Vincent. [Des professionnel·le·s de l’informatique deviennent enseignant·e·s](https://www.lemonde.fr/blog/binaire/2022/10/07/des-professionnel%25c2%25b7le%25c2%25b7s-de-linformatique-deviennent-enseignant%25c2%25b7e%25c2%25b7s), Blog binaire sur LeMonde.fr, Octobre 2022

(collectif)  [La NSI pour que tout le monde y arrive aussi](https://www.lemonde.fr/blog/binaire/2022/04/29/la-nsi-pour-que-tout-le-monde-y-arrive-aussi) Blog binaire sur LeMonde.fr, Avril 2022


<script src="./assets/make_foldable.js"/>
